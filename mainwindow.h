#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore>
#include <qvector.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    double f;
    QVector<double> xVector;
    QVector<double> yVector;
    QVector<double> zVector;
    QVector< QVector<int> > p;
    QVector< QVector<double> > v;
    QVector<double> tx, ty, tz;

    int getXe(double y, double z);
    int getYe(double x, double z);
    void initTemps();
    void processCoordinates();
    void moveFigure(double dx, double dy, double dz, boolean repaint);
    void moveFigure(double dx, double dy, double dz);
    QVector< QVector<double> > multiply(double matrix[4][4], QVector< QVector<double> > v);
    double findCX();
    double findCY();
    double findCZ();
    void rotateFigureZ(double d);
    void rotateFigureX(double d);
    void rotateFigureY(double d);

protected:
    void paintEvent(QPaintEvent *e);
private slots:
    void on_pushButton_left_clicked();
    void on_pushButton_up_clicked();
    void on_pushButton_right_clicked();
    void on_pushButton_down_clicked();
    void on_pushButton_plus_clicked();
    void on_pushButton_minus_clicked();
    void on_pushButton_rotatey_clicked();
    void on_pushButton_rotatedy_clicked();
    void on_pushButton_rotatex_clicked();
    void on_pushButton_rotatedx_clicked();
    void on_pushButton_rotatez_clicked();
    void on_pushButton_rotatedz_clicked();
};

#endif // MAINWINDOW_H
