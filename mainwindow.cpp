#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qmath.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    f = 600;
    double x[] = {
                    275, 425, 425, 275, 275, 425, 425, 275, //fourth
                    275, 425, 425, 275, 275, 425, 425, 275, //third
                    100, 250, 250, 100, 100, 250, 250, 100, //second
                    100, 250, 250, 100, 100, 250, 250, 100  //first
                 };
    double y[] = {
                    275, 275, 425, 425, 275, 275, 425, 425, //fourth
                    100, 100, 250, 250, 100, 100, 250, 250, //third
                    275, 275, 425, 425, 275, 275, 425, 425, //second
                    100, 100, 250, 250, 100, 100, 250, 250  //first
                 };
    double z[] = {
                    500, 500, 500, 500, 750, 750, 750, 750, //fourth
                    500, 500, 500, 500, 750, 750, 750, 750, //third
                    500, 500, 500, 500, 750, 750, 750, 750, //second
                    500, 500, 500, 500, 750, 750, 750, 750  //first
                 };

    double vBuf[4][4] = {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    std::vector<double> bufVector;
    bufVector.assign(x, x + sizeof(x)/sizeof(double));
    xVector = QVector<double>::fromStdVector(bufVector);

    bufVector.clear();
    bufVector.assign(y, y + sizeof(y)/sizeof(double));
    yVector = QVector<double>::fromStdVector(bufVector);

    bufVector.clear();
    bufVector.assign(z, z + sizeof(z)/sizeof(double));
    zVector = QVector<double>::fromStdVector(bufVector);

    v = QVector< QVector<double> >(4);
    for (int i = 0; i < 4; i++)
    {
        bufVector.clear();
        bufVector.assign(vBuf[i], vBuf[i] + sizeof(vBuf[i])/sizeof(double));
        v[i] = QVector<double>::fromStdVector(bufVector);
    }

    initTemps();
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::getXe(double y, double z)
{
    return (int) (f * y / z);
}

int MainWindow::getYe(double x, double z)
{
    return (int) (f * x / z);
}

void MainWindow::initTemps()
{
    tx = QVector<double>(xVector.size());
    ty = QVector<double>(xVector.size());
    tz = QVector<double>(xVector.size());
    processCoordinates();
}

void MainWindow::processCoordinates()
{
    for (int i = 0; i < xVector.size(); i++)
    {
        tx[i] = v[0][0] * xVector[i] + v[0][1] * yVector[i] + v[0][2] * zVector[i] + v[0][3];
        ty[i] = v[1][0] * xVector[i] + v[1][1] * yVector[i] + v[1][2] * zVector[i] + v[1][3];
        tz[i] = v[2][0] * xVector[i] + v[2][1] * yVector[i] + v[2][2] * zVector[i] + v[2][3];
    }
}

void MainWindow::moveFigure(double dx, double dy, double dz, boolean repaint)
{
    double matrix[4][4] = {
                {1,0,0,dx},
                {0,1,0,dy},
                {0,0,1,dz},
                {0,0,0,1},
            };
    v = multiply(matrix, v);
    if (repaint) {
        processCoordinates();
        this->update();
    }
}

void MainWindow::moveFigure(double dx, double dy, double dz)
{
    moveFigure(dx, dy, dz, true);
}

QVector<QVector<double> > MainWindow::multiply(double matrix[4][4], QVector<QVector<double> > v)
{
    QVector< QVector<double> > result = QVector< QVector<double> >(4, QVector<double>(4));
    for (int i = 0; i < result.size(); i++)
    {
        for (int j = 0; j < result[i].size(); j++)
        {
            for (int k = 0; k < result.size(); k++)
            {
                result[i][j] += matrix[i][k] * v[k][j];
            }
        }
    }
    return result;
}

double MainWindow::findCX()
{
    double result = tx[0];
    for (int i = 1; i < tx.size(); i++)
        result += tx[i];
    return result/tx.size();
}

double MainWindow::findCY()
{
    double result = tx[0];
    for (int i = 1; i < ty.size(); i++)
        result += ty[i];
    return result/ty.size();
}

double MainWindow::findCZ()
{
    double result = tz[0];
    for (int i = 1; i < tz.size(); i++)
        result += tz[i];
    return result/tz.size();
}

void MainWindow::rotateFigureZ(double d)
{
    double alpha = qDegreesToRadians(d);
    double matrix[4][4] = {
        {qCos(alpha), -qSin(alpha), 0, 0},
        {qSin(alpha), qCos(alpha), 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };
    double centerX, centerY, centerZ;
    centerX = findCX();
    centerY = findCY();
    centerZ = findCZ();
    moveFigure(-centerX, -centerY, -centerZ, false);
    v = multiply(matrix, v);
    moveFigure(centerX, centerY, centerZ, false);
    processCoordinates();
    this->update();
}

void MainWindow::rotateFigureX(double d)
{
    double alpha = qDegreesToRadians(d);
    double matrix[4][4] = {
        {1, 0, 0, 0},
        {0, qCos(alpha), -qSin(alpha), 0},
        {0, qSin(alpha), qCos(alpha), 0},
        {0, 0, 0, 1}
    };
    double centerX, centerY, centerZ;
    centerX = findCX();
    centerY = findCY();
    centerZ = findCZ();
    moveFigure(-centerX, -centerY, -centerZ, false);
    v = multiply(matrix, v);
    moveFigure(centerX, centerY, centerZ, false);
    processCoordinates();
    this->update();
}

void MainWindow::rotateFigureY(double d)
{
    double alpha = qDegreesToRadians(d);
    double matrix[4][4] = {
        {qCos(alpha), 0, qSin(alpha), 0},
        {0, 1, 0, 0},
        {-qSin(alpha), 0, qCos(alpha), 0},
        {0, 0, 0, 1}
    };
    double centerX, centerY, centerZ;
    centerX = findCX();
    centerY = findCY();
    centerZ = findCZ();
    moveFigure(-centerX, -centerY, -centerZ, false);
    v = multiply(matrix, v);
    moveFigure(centerX, centerY, centerZ, false);
    processCoordinates();
    this->update();
}

void MainWindow::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);

    //Make a pen
    QPen linePen;
    linePen.setWidth(4);
    linePen.setColor(Qt::black);
    painter.setPen(linePen);

    //Make a brush
    QBrush fillBrush;
    fillBrush.setColor(Qt::red);
    fillBrush.setStyle(Qt::SolidPattern);


    int p[6][4] = {
            {0,3,7,4},
            {1,5,6,2},
            {3,2,6,7},
            {5,4,7,6},
            {0,4,5,1},
            {0,1,2,3}
        };

    for (int cubeNumber = 0; cubeNumber < 4; cubeNumber++)
    {
        fillBrush.setColor( (cubeNumber==3) ? (Qt::red)
                                            : (cubeNumber==2) ? (Qt::yellow)
                                            : (cubeNumber==1) ? (Qt::green)
                                            : (Qt::blue) );
        for (int i = 0; i < 6; i++)
        {
            //Make a polygon
            QPolygon poly;

            for (int j = 0; j < 4; j++){
                int offset = cubeNumber * 8;
                poly << QPoint( getXe(tx[p[i][j]+offset], tz[p[i][j]+offset]),
                        getYe(ty[p[i][j]+offset], tz[p[i][j]+offset]) );
            }

            //Draw polygon
            QPainterPath path;
            path.addPolygon(poly);
            painter.drawPolygon(poly);
            painter.fillPath(path, fillBrush);
        }
    }

}

void MainWindow::on_pushButton_left_clicked()
{
    moveFigure(-5, 0, 0);
}

void MainWindow::on_pushButton_up_clicked()
{
    moveFigure(0, -5, 0);
}

void MainWindow::on_pushButton_right_clicked()
{
    moveFigure(5, 0, 0);
}

void MainWindow::on_pushButton_down_clicked()
{
    moveFigure(0, 5, 0);
}

void MainWindow::on_pushButton_plus_clicked()
{
    f+=10;
    this->update();
}

void MainWindow::on_pushButton_minus_clicked()
{
    f-=10;
    this->update();
}

void MainWindow::on_pushButton_rotatey_clicked()
{
    rotateFigureY(45);
}

void MainWindow::on_pushButton_rotatedy_clicked()
{
    rotateFigureY(-45);
}

void MainWindow::on_pushButton_rotatex_clicked()
{
    rotateFigureX(45);
}

void MainWindow::on_pushButton_rotatedx_clicked()
{
    rotateFigureX(-45);
}

void MainWindow::on_pushButton_rotatez_clicked()
{
    rotateFigureZ(45);
}

void MainWindow::on_pushButton_rotatedz_clicked()
{
    rotateFigureZ(-45);
}
